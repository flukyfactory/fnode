/*
	Server JS
	Developers:
		Luis Matute 		- luis.matute@me.com
		Adolfo Gutierrez	- adolfo.gutierrez.87@gmail.com
	Description:
		This is the server configuration.
*/

// Module Dependencies and App Instantiation
var
	express 	= require('express')

	// Instantiate Express
	app 	= express(),
	env 	= app.get('env')

// Env Settings
	app.locals( require('./config/environments')[env] )

// App settings
	require('./config/express')(app)

// Fire up the Server
	app.listen(app.locals.port, function () {
    console.log(
	  	'Express server listening on port ' + app.locals.port + '\n'
	  	+ 'With ' + env.toUpperCase() + ' Environment settings loaded'
	  )
	})

// Mongo